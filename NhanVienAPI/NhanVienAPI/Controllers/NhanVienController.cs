﻿using NhanVienAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NhanVienAPI.Controllers
{
    public class NhanVienController : ApiController
    {
        // GET: api/NhanVien
        public string connectionString = @"Data Source=PHONGNHA\SQLEXPRESS;Initial Catalog=QLNhan_Vien;Integrated Security=True";

        [Route("list-nhanvien")]
        public IEnumerable<NhanVien> Get()
        {
            var listNhanVien = new List<NhanVien>();
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            //khoi tao doi tuong
            SqlCommand sqlCommand = sqlConnection.CreateCommand();
            //sqlCommand.CommandText = "select * from employee"; // dùng kiểu text để truy vấn dữ liệu
            //dùng stored PROCEDURE
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            sqlCommand.CommandText = "dbo.nv_nhanviens"; //gọi tên của procedure vào
            //start connection
            sqlConnection.Open();
            //Thực thi công việc với câu lệnh sql
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            //Xử lý dữ liệu trả về
            while (sqlDataReader.Read())
            {

                var nhanvien = new NhanVien();
                for (int i = 0; i < sqlDataReader.FieldCount; i++)
                {
                    //Lấy tên cột dữ liệu đang đọc
                    //lưu ý colName ở class phải giống tên cột csdl
                    var colName = sqlDataReader.GetName(i);
                    //Lấy value cảu cột đó
                    var value = sqlDataReader.GetValue(i);
                    //Lấy ra property giống với tên cột đã khai báo ở trên
                    var property = nhanvien.GetType().GetProperty(colName);

                    //Nếu có property tương ứng với cột sẽ gán cho giá trị tương ứng
                    if (property != null && value != DBNull.Value)
                    {
                        property.SetValue(nhanvien, value);
                    }
                }
                listNhanVien.Add(nhanvien);
            }
            //finish connecttion
            sqlConnection.Close();

            return listNhanVien;
        }

        // GET: api/NhanVien/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/NhanVien
        [Route("add-NhanVien")]
        public bool Post([FromBody]NhanVien nhanvien)
        {
           
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            //khoi tao doi tuong
            SqlCommand sqlCommand = sqlConnection.CreateCommand();
            //dùng stored PROCEDURE
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            sqlCommand.CommandText = "dbo.nv_insert_NhanVien"; //gọi tên của procedure vào
            //start connection
            sqlConnection.Open();
            //gán giá trị cho biến trong stored procdure
            sqlCommand.Parameters.AddWithValue("@HoTen", nhanvien.HoTen);
            sqlCommand.Parameters.AddWithValue("@GioiTinh", nhanvien.GioiTinh);
            sqlCommand.Parameters.AddWithValue("@DienThoai", nhanvien.DienThoai);
            sqlCommand.Parameters.AddWithValue("@NgaySinh", DateTime.Now);
          
            //Thực thi công việc với câu lệnh sql
            var result = sqlCommand.ExecuteNonQuery();
            sqlConnection.Close();
            return true;
        }

        // PUT: api/NhanVien/5
        [Route("update-NhanVien")]
        public bool Put([FromBody] NhanVien nhanvien)
        {
            
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            //khoi tao doi tuong
            SqlCommand sqlCommand = sqlConnection.CreateCommand();
            //dùng stored PROCEDURE
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            sqlCommand.CommandText = "dbo.nv_update_NhanVien"; //gọi tên của procedure vào
            //start connection
            sqlConnection.Open();
            //gán giá trị cho biến trong stored procdure
            sqlCommand.Parameters.AddWithValue("@id", nhanvien.id);
            sqlCommand.Parameters.AddWithValue("@HoTen", nhanvien.HoTen);
            sqlCommand.Parameters.AddWithValue("@GioiTinh", nhanvien.GioiTinh);
            sqlCommand.Parameters.AddWithValue("@DienThoai", nhanvien.DienThoai);
            sqlCommand.Parameters.AddWithValue("@NgaySinh", nhanvien.NgaySinh);

            //Thực thi công việc với câu lệnh sql
            var result = sqlCommand.ExecuteNonQuery();
            sqlConnection.Close();
            return true;
        }

        // DELETE: api/NhanVien/5
        [Route("delete-NhanVien")]
        public bool Delete([FromBody] NhanVien nhanvien)
        {
           
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            //khoi tao doi tuong
            SqlCommand sqlCommand = sqlConnection.CreateCommand();
            //dùng stored PROCEDURE
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            sqlCommand.CommandText = "dbo.nv_delete_NhanVien"; //gọi tên của procedure vào
            //start connection
            sqlConnection.Open();
            //gán giá trị cho biến trong stored procdure
            sqlCommand.Parameters.AddWithValue("@id", nhanvien.id);
            //Thực thi công việc với câu lệnh sql
            var result = sqlCommand.ExecuteNonQuery();
            sqlConnection.Close();
            return true;
        }
    }
}
