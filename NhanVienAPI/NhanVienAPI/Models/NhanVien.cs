﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NhanVienAPI.Models
{
    public class NhanVien
    {
        public int id { get; set; }
        public string HoTen { get; set; }
        public int GioiTinh { get; set; }
        public string DienThoai { get; set; }
        public DateTime NgaySinh { get; set; }
    }
}